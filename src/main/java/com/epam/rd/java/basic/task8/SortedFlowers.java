package com.epam.rd.java.basic.task8;

import java.util.Collections;
import java.util.stream.Collectors;

import static java.util.Collections.*;

public class SortedFlowers extends Flowers{

    public SortedFlowers() {
    }

    public SortedFlowers(Flowers flowers) {
        this.flower = flowers.flower;
    }

    public void sortByName() {
        this.flower.sort((a, b) -> a.getName().compareToIgnoreCase(b.getName()));
    }

    public void sortByOrigin() {
        sort(this.flower, (a, b) -> a.getOrigin().compareToIgnoreCase(b.getOrigin()));
    }

    public void sortByLeafColour() {
        sort(this.flower, (a, b) -> a.getVisualParameters().getLeafColour().compareToIgnoreCase(b.getVisualParameters().getLeafColour()));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("SortedFlowers{\n");
        for (Flower fl: flower) {
            sb.append("\t").append(fl.name).append("\n");
        }
        sb.append('}');
        return sb.toString();
    }
}
