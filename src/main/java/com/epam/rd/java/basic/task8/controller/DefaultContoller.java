package com.epam.rd.java.basic.task8.controller;

public interface DefaultContoller {
    String ATTR_MEASURE = "measure";
    String ATTR_LIGHT_REQUIRING = "lightRequiring";
    String FLOWERS = "flowers";
    String FLOWER = "flower";
    String NAME = "name";
    String SOIL = "soil";
    String ORIGIN = "origin";
    String VISUAL_PARAMETERS = "visualParameters";
    String STEM_COLOUR = "stemColour";
    String LEAF_COLOUR = "leafColour";
    String AVE_LEN_FLOWER = "aveLenFlower";
    String GROWING_TIPS = "growingTips";
    String TEMPERATURE = "tempreture";
    String LIGHTNING = "lighting";
    String WATERING = "watering";
    String MULTIPLYING = "multiplying";

}
