package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flowers;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.math.BigInteger;

/**
 * Controller for DOM parser.
 */
public class DOMController implements DefaultContoller {

    private final String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public Flowers readXML(Document doc) {


        Flowers flowers = new Flowers();
//		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
        NodeList nList = doc.getElementsByTagName(FLOWER);

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            Flowers.Flower flower = new Flowers.Flower();

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                flower.setName(eElement
                        .getElementsByTagName(NAME)
                        .item(0)
                        .getTextContent());
                flower.setSoil(eElement
                        .getElementsByTagName(SOIL)
                        .item(0)
                        .getTextContent());
                flower.setOrigin(eElement
                        .getElementsByTagName(ORIGIN)
                        .item(0)
                        .getTextContent());
                Flowers.Flower.VisualParameters visualParameter = new Flowers.Flower.VisualParameters();
                visualParameter.setStemColour(eElement
                        .getElementsByTagName(STEM_COLOUR)
                        .item(0)
                        .getTextContent());
                visualParameter.setLeafColour(eElement
                        .getElementsByTagName(LEAF_COLOUR)
                        .item(0)
                        .getTextContent());
                Flowers.Flower.VisualParameters.AveLenFlower aveLenFlower = new Flowers.Flower.VisualParameters.AveLenFlower();
                Node aveNode = eElement
                        .getElementsByTagName(AVE_LEN_FLOWER)
                        .item(0);
                aveLenFlower.setMeasure(aveNode.getAttributes().getNamedItem(ATTR_MEASURE).getNodeValue());
                aveLenFlower.setValue(new BigInteger(aveNode.getTextContent()));
                visualParameter.setAveLenFlower(aveLenFlower);
                flower.setVisualParameters(visualParameter);
                Flowers.Flower.GrowingTips growingTips = new Flowers.Flower.GrowingTips();
                Flowers.Flower.GrowingTips.Tempreture tempreture = new Flowers.Flower.GrowingTips.Tempreture();
                Node tempNode = eElement
                        .getElementsByTagName(TEMPERATURE)
                        .item(0);
                tempreture.setMeasure(tempNode.getAttributes().getNamedItem(ATTR_MEASURE).getNodeValue());
                tempreture.setValue(new BigInteger(tempNode
                        .getTextContent()));
                growingTips.setTempreture(tempreture);
                Flowers.Flower.GrowingTips.Lighting lighting = new Flowers.Flower.GrowingTips.Lighting();
                lighting.setLightRequiring(eElement.getElementsByTagName(LIGHTNING).item(0).getAttributes()
                        .getNamedItem(ATTR_LIGHT_REQUIRING).getNodeValue());
                growingTips.setLighting(lighting);
                Flowers.Flower.GrowingTips.Watering watering = new Flowers.Flower.GrowingTips.Watering();
                Node waterNode = eElement.getElementsByTagName(WATERING).item(0);
                watering.setMeasure(waterNode.getAttributes().getNamedItem(ATTR_MEASURE).getNodeValue());
                watering.setValue(new BigInteger(waterNode.getTextContent()));
                growingTips.setWatering(watering);
                flower.setGrowingTips(growingTips);
                flower.setMultiplying(eElement
                        .getElementsByTagName(MULTIPLYING)
                        .item(0)
                        .getTextContent());
            }
            flowers.getFlower().add(flower);
        }
        return flowers;
    }


}
