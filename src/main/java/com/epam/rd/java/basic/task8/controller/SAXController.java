package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flowers;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import java.math.BigInteger;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler implements DefaultContoller{

    String xmlFileName;
    private Flowers flowerList;
    private Flowers.Flower.VisualParameters visualParameterTag;
    private Flowers.Flower.VisualParameters.AveLenFlower aveLenFlowerTag;
    private Flowers.Flower.GrowingTips growingTipsTag;
    private Flowers.Flower.GrowingTips.Lighting lightingTag;
    private Flowers.Flower.GrowingTips.Tempreture tempretureTag;
    private Flowers.Flower.GrowingTips.Watering wateringTag;

    private StringBuilder elementValue;


    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public void startDocument() {
        flowerList = new Flowers();
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        if (elementValue == null) {
            elementValue = new StringBuilder();
        } else {
            elementValue.append(ch, start, length);
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {

        switch (qName) {
            case FLOWER:
                flowerList.getFlower().add(new Flowers.Flower());
                break;
            case NAME:
            case SOIL:
            case ORIGIN:
            case STEM_COLOUR:
            case LEAF_COLOUR:
            case MULTIPLYING:
                elementValue = new StringBuilder();
                break;
            case VISUAL_PARAMETERS:
                visualParameterTag = new Flowers.Flower.VisualParameters();
                break;
            case AVE_LEN_FLOWER:
                aveLenFlowerTag = new Flowers.Flower.VisualParameters.AveLenFlower();
                aveLenFlowerTag.setMeasure(attributes.getValue(ATTR_MEASURE));
                elementValue = new StringBuilder();
                break;
            case GROWING_TIPS:
                growingTipsTag = new Flowers.Flower.GrowingTips();
                break;
            case TEMPERATURE:
                elementValue = new StringBuilder();
                tempretureTag = new Flowers.Flower.GrowingTips.Tempreture();
                tempretureTag.setMeasure(attributes.getValue(ATTR_MEASURE));
                break;
            case LIGHTNING:
                lightingTag = new Flowers.Flower.GrowingTips.Lighting();
                lightingTag.setLightRequiring(attributes.getValue(ATTR_LIGHT_REQUIRING));
            case WATERING:
                wateringTag = new Flowers.Flower.GrowingTips.Watering();
                wateringTag.setMeasure(attributes.getValue(ATTR_MEASURE));
                elementValue = new StringBuilder();
                break;
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        switch (qName) {
            case NAME:
                latestFlower().setName(elementValue.toString());
                break;
            case SOIL:
                latestFlower().setSoil(elementValue.toString());
                break;
            case ORIGIN:
                latestFlower().setOrigin(elementValue.toString());
                break;
            case VISUAL_PARAMETERS:
                latestFlower().setVisualParameters(visualParameterTag);
                break;
            case STEM_COLOUR:
                visualParameterTag.setStemColour(elementValue.toString());
                break;
            case LEAF_COLOUR:
                visualParameterTag.setLeafColour(elementValue.toString());
                break;
            case AVE_LEN_FLOWER:
                aveLenFlowerTag.setValue(new BigInteger(elementValue.toString()));
                visualParameterTag.setAveLenFlower(aveLenFlowerTag);
                break;
            case GROWING_TIPS:
                growingTipsTag.setLighting(lightingTag);
                growingTipsTag.setTempreture(tempretureTag);
                growingTipsTag.setWatering(wateringTag);
                latestFlower().setGrowingTips(growingTipsTag);
                break;
            case TEMPERATURE:
                tempretureTag.setValue(new BigInteger(elementValue.toString()));
                break;
            case WATERING:
                wateringTag.setValue(new BigInteger(elementValue.toString()));
                break;
            case MULTIPLYING:
                latestFlower().setMultiplying(elementValue.toString());
                break;
        }
    }

    private Flowers.Flower latestFlower() {
        List<Flowers.Flower> flowerList = this.flowerList.getFlower();
        int latestFlowerIndex = flowerList.size() - 1;
        return flowerList.get(latestFlowerIndex);
    }

    public Flowers getFlowerList() {
        return flowerList;
    }


}