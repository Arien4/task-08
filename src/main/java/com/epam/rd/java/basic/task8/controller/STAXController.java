package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flowers;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.math.BigInteger;

/**
 * Controller for StAX parser.
 */
public class STAXController implements DefaultContoller {
    private final String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public Flowers readXML(XMLEventReader reader) throws XMLStreamException {
        Flowers flowers = new Flowers();
        Flowers.Flower flower = new Flowers.Flower();
        Flowers.Flower.VisualParameters visualParameter = new Flowers.Flower.VisualParameters();
        Flowers.Flower.VisualParameters.AveLenFlower aveLenFlower = new Flowers.Flower.VisualParameters.AveLenFlower();
        Flowers.Flower.GrowingTips growingTips = new Flowers.Flower.GrowingTips();
        Flowers.Flower.GrowingTips.Lighting lighting = null;
        Flowers.Flower.GrowingTips.Tempreture tempreture = null;
        Flowers.Flower.GrowingTips.Watering watering = null;

        Attribute attr;
        while (reader.hasNext()) {
            XMLEvent nextEvent = reader.nextEvent();
            if (nextEvent.isStartElement()) {
                StartElement startElement = nextEvent.asStartElement();

                switch (startElement.getName().getLocalPart()) {
                    case FLOWER:
                        flower = new Flowers.Flower();
                        break;
                    case NAME:
                        nextEvent = reader.nextEvent();
                        flower.setName(nextEvent.asCharacters().getData());
                        break;
                    case SOIL:
                        nextEvent = reader.nextEvent();
                        flower.setSoil(nextEvent.asCharacters().getData());
                        break;
                    case ORIGIN:
                        nextEvent = reader.nextEvent();
                        flower.setOrigin(nextEvent.asCharacters().getData());
                        break;
                    case VISUAL_PARAMETERS:
                        visualParameter = new Flowers.Flower.VisualParameters();
                        break;
                    case STEM_COLOUR:
                        nextEvent = reader.nextEvent();
                        visualParameter.setStemColour(nextEvent.asCharacters().getData());
                        break;
                    case LEAF_COLOUR:
                        nextEvent = reader.nextEvent();
                        visualParameter.setLeafColour(nextEvent.asCharacters().getData());
                        break;
                    case GROWING_TIPS:
                        growingTips = new Flowers.Flower.GrowingTips();
                        break;
                    case AVE_LEN_FLOWER:
                        aveLenFlower = new Flowers.Flower.VisualParameters.AveLenFlower();
                        attr = startElement.getAttributeByName(new QName(ATTR_MEASURE));
                        if (attr != null) aveLenFlower.setMeasure(attr.getValue());
                        nextEvent = reader.nextEvent();
                        aveLenFlower.setValue(new BigInteger(nextEvent.asCharacters().getData()));
                        break;
                    case TEMPERATURE:
                        tempreture = new Flowers.Flower.GrowingTips.Tempreture();
                        attr = startElement.getAttributeByName(new QName(ATTR_MEASURE));
                        if (attr != null) tempreture.setMeasure(attr.getValue());
                        nextEvent = reader.nextEvent();
                        tempreture.setValue(new BigInteger(nextEvent.asCharacters().getData()));
                        break;
                    case LIGHTNING:
                        lighting = new Flowers.Flower.GrowingTips.Lighting();
                        attr = startElement.getAttributeByName(new QName(ATTR_LIGHT_REQUIRING));
                        if (attr != null) lighting.setLightRequiring(attr.getValue());
                        break;
                    case WATERING:
                        watering = new Flowers.Flower.GrowingTips.Watering();
                        attr = startElement.getAttributeByName(new QName(ATTR_MEASURE));
                        if (attr != null) watering.setMeasure(attr.getValue());
                        nextEvent = reader.nextEvent();
                        watering.setValue(new BigInteger(nextEvent.asCharacters().getData()));
                        break;
                    case MULTIPLYING:
                        nextEvent = reader.nextEvent();
                        flower.setMultiplying(nextEvent.asCharacters().getData());
                        break;
                }

            }
            if (nextEvent.isEndElement()) {
                EndElement endElement = nextEvent.asEndElement();
                switch (endElement.getName().getLocalPart()) {
                    case FLOWER:
                        flowers.getFlower().add(flower);
                        break;
                    case VISUAL_PARAMETERS:
                        visualParameter.setAveLenFlower(aveLenFlower);
                        flower.setVisualParameters(visualParameter);
                        break;
                    case GROWING_TIPS:
                        growingTips.setWatering(watering);
                        growingTips.setTempreture(tempreture);
                        growingTips.setLighting(lighting);
                        flower.setGrowingTips(growingTips);
                        break;
                }

            }
        }
        return flowers;
    }



}