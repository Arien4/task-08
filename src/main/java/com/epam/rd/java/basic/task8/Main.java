package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.DOMController;
import com.epam.rd.java.basic.task8.controller.SAXController;
import com.epam.rd.java.basic.task8.controller.STAXController;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import java.io.*;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }
        //String xmlFileName = "input.xml";
        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        File inputFile = new File(xmlFileName);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();
        Flowers domResult = domController.readXML(doc);
        SortedFlowers sortedDomResult = new SortedFlowers(domResult);
        sortedDomResult.sortByName();
        String outputXmlFile = "output.dom.xml";
        flowersToXMLFile(outputXmlFile, sortedDomResult);


        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        SAXController saxController = new SAXController(xmlFileName);
        saxParser.parse(xmlFileName, saxController);
        Flowers saxResult = saxController.getFlowerList();
        SortedFlowers sortedResult = new SortedFlowers(saxResult);
        sortedResult.sortByLeafColour();
        outputXmlFile = "output.sax.xml";
        flowersToXMLFile(outputXmlFile, sortedResult);

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
        STAXController staxController = new STAXController(xmlFileName);
        Flowers staxResult = staxController.readXML(reader);
        SortedFlowers sortedStaxFlowers = new SortedFlowers(staxResult);
        sortedStaxFlowers.sortByOrigin();
        outputXmlFile = "output.stax.xml";
        flowersToXMLFile(outputXmlFile, sortedStaxFlowers);
    }

    private static void flowersToXMLFile(String outputXmlFile, Flowers flowers) throws IOException, JAXBException {
        StringWriter writer = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(Flowers.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(flowers, writer);
        try (FileWriter myWriter = new FileWriter(outputXmlFile)) {
            myWriter.write(writer.toString());
        }

    }


}
